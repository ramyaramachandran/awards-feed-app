import { Component } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from './api.service';
import { Award } from './award';
import { NgxXml2jsonService } from 'ngx-xml2json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [NgbCarouselConfig]
})

export class AppComponent {
  title = 'awards-feed-app';
  showNavigationArrows = false;
  showNavigationIndicators = false;
  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
  awards: Array<Award> = [];

  constructor(config: NgbCarouselConfig, private apiService: ApiService, private ngxXml2jsonService: NgxXml2jsonService) {
    //nav Properties
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;

    this.apiService.getAwards().subscribe(res => {
      //At the moment team hq not returning success response
      console.log("Success Response");     
    }, err => {
      //Parsing rss feed to json
      const parser = new DOMParser();
      const xml = parser.parseFromString(err.error.text, 'text/xml'); 
      let awardsRss = <any>{}; 
      awardsRss = this.ngxXml2jsonService.xmlToJson(xml);  
      this.awards = awardsRss.rss.channel.item;
      console.log(this.awards);
    });    
  }
}
