import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiURL: string = 'http://dv1-teamhq.afgonline.com.au/category/awards';//
  constructor(private httpClient: HttpClient) { }
  getAwards() {
    var awardsPromise = this.httpClient.get<[]>(`${this.apiURL}/feed/`);
    console.log(awardsPromise);
    return awardsPromise;
  }
}

